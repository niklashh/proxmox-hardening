# Usage

Override ansible_host in the inventory and run

```sh
ansible-playbook -vv host.yml --limit production --ask-become-pass -e "pre_host=..."
```

> Do not pass in the ip address of the server in extra vars

# Development

Running vagrant up provisions the host with `host.yml`
